package com.springboot.mortgage.service;


import java.util.List;

import com.springboot.mortgage.model.LoanDetails;
import com.springboot.mortgage.model.RateDetails;

public interface MortgageService {
	
	List<RateDetails> findInterestRates();
	
	boolean loanApproved(LoanDetails loanDetails);
	
}
