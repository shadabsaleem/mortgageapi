package com.springboot.mortgage.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.springboot.mortgage.model.RateDetails;

@Repository
public class MortgageRepository {
	
	@Value("#{'${db.h2.rate.query}'.trim()}")
	private String query;
	
	public static final Logger logger = LoggerFactory.getLogger(MortgageRepository.class);
	
	@Autowired
    JdbcTemplate jdbcTemplate;
	
	public List<RateDetails> getRates() {		
		logger.debug("Executing Query:{}",query);
	    return jdbcTemplate.queryForList(query,null, RateDetails.class);	        
	}
}
