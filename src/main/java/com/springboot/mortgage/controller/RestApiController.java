package com.springboot.mortgage.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.mortgage.model.LoanDetails;
import com.springboot.mortgage.model.RateDetails;
import com.springboot.mortgage.service.MortgageService;

@RestController
@RequestMapping("/api")
public class RestApiController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	MortgageService mortgageService;

	// -------------------Retrieve All Interest Rates---------------------------------------------

	@RequestMapping(value = "/interest-rates/", method = RequestMethod.GET)
	public ResponseEntity<List<RateDetails>> getInterestRates() {
		logger.debug("Executing:getInterestRates()");
		List<RateDetails> rates = mortgageService.findInterestRates();
		logger.debug("getInterestRates() returns : "+rates.size()+" rates");
		if (rates.isEmpty()) {
			logger.debug("No rates available");
			return new ResponseEntity(HttpStatus.NO_CONTENT);			
		}
		return new ResponseEntity<List<RateDetails>>(rates, HttpStatus.OK);
	}

	// -------------------Loan Eligibility-------------------------------------------

	@RequestMapping(value = "/mortgage-check/", method = RequestMethod.POST)
	@ResponseBody
	public Boolean checkMortgage(@RequestBody LoanDetails loanDetails) {
	  double zeroVal = 0.0;
	  logger.debug("Executing:checkMortgage()");
	  if(loanDetails.getHomeValue()==zeroVal||loanDetails.getIncome()==zeroVal||
	      loanDetails.getLoanValue()==zeroVal||loanDetails.getMaturityPeriod()==0){
	    logger.error("Invalid input: Missing required information");
	    return false;
	    
	  }
	  boolean isLoanApproved = mortgageService.loanApproved(loanDetails);
	  logger.debug("checkMortgage():Is loan approved: "+isLoanApproved);
	  return isLoanApproved;		
	}

}