package com.springboot.mortgage.serviceimpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.mortgage.controller.RestApiController;
import com.springboot.mortgage.model.LoanDetails;
import com.springboot.mortgage.model.RateDetails;
import com.springboot.mortgage.service.MortgageRepository;
import com.springboot.mortgage.service.MortgageService;

@Service
public class MortgageServiceImpl implements MortgageService{
	
  public static final Logger logger = LoggerFactory.getLogger(MortgageServiceImpl.class);
	
  @Autowired
  RateDetails rateDetail;
  
  @Autowired
  MortgageRepository repo;
	
  private static List<RateDetails> rates;


  @Override
  public List<RateDetails> findInterestRates() {
	logger.debug("Fetching rates from H2 database()");
	return repo.getRates();
  }

@Override
  public boolean loanApproved(LoanDetails loanDetails) {
    if(loanDetails.getHomeValue()>(loanDetails.getLoanValue())||
        loanDetails.getLoanValue()>4*(loanDetails.getIncome())){
      return false;
    }
    return true;
  }

}
