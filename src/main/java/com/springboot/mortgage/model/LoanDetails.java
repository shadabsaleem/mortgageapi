package com.springboot.mortgage.model;

import org.springframework.stereotype.Component;

@Component
public class LoanDetails {

	private double income;
	
	private int maturityPeriod ;
	
	private double loanValue;
	
	private double homeValue ;

	public LoanDetails(){
	  income=0.0;
	  maturityPeriod=0;
	  loanValue=0.0;
	  homeValue=0.0;
	}
	
	public LoanDetails(double income, double loanValue, int maturityPeriod, double homeValue){
		this.income = income;
		this.loanValue = loanValue;
		this.maturityPeriod = maturityPeriod;
		this.homeValue = homeValue;
	}


	public double getIncome() {
    return income;
  }

  public void setIncome(double income) {
    this.income = income;
  }

  public int getMaturityPeriod() {
    return maturityPeriod;
  }

  public void setMaturityPeriod(int maturityPeriod) {
    this.maturityPeriod = maturityPeriod;
  }

  public double getLoanValue() {
    return loanValue;
  }

  public void setLoanValue(double loanValue) {
    this.loanValue = loanValue;
  }

  public double getHomeValue() {
    return homeValue;
  }

  public void setHomeValue(double homeValue) {
    this.homeValue = homeValue;
  }

}
