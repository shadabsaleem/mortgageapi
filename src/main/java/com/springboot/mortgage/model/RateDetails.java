package com.springboot.mortgage.model;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

@Component
public class RateDetails {

	private int maturityPeriod ;
	
	private double rate;
	
	private Timestamp currentTime;
	

	public RateDetails(){
	  rate=0.0;
	  maturityPeriod=0;	  
	}
	
	public RateDetails(int maturityPeriod, double rate){
		this.maturityPeriod = maturityPeriod;
		this.rate = rate;
	}

	public int getMaturityPeriod() {
		return maturityPeriod;
	}

	public void setMaturityPeriod(int maturityPeriod) {
		this.maturityPeriod = maturityPeriod;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public Timestamp getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(Timestamp currentTime) {
		this.currentTime = currentTime;
	}	
	
}
