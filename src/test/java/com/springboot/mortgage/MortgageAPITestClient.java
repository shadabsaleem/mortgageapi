package com.springboot.mortgage;
 
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.springboot.mortgage.model.LoanDetails;
import com.springboot.mortgage.model.RateDetails;
 

public class MortgageAPITestClient {
 
    public static final String REST_SERVICE_URI = "http://localhost:8080/MortgageAPI/api";
     
    @SuppressWarnings("unchecked")
    private static void getInterestRates(){
        System.out.println("Get today's interst rates");
         
        RestTemplate restTemplate = new RestTemplate();
        List<RateDetails> rates = restTemplate.getForObject(REST_SERVICE_URI+"/interest-rates/", List.class);         
        
        for(RateDetails rate : rates){
                System.out.println("Interest Rate:"+rate.getRate());
                System.out.println("Maturity Period:"+rate.getMaturityPeriod());
                System.out.println("Current Time:"+rate.getCurrentTime());
            }        
    }    
     
    private static void loanApproved() {
        System.out.println("Verify loan approval----------");
        RestTemplate restTemplate = new RestTemplate();
        LoanDetails user = new LoanDetails(1000.50,1000,20,1500);
        System.out.println("Loan Approved : "+restTemplate.postForLocation(REST_SERVICE_URI+"/mortgage-check/", user, LoanDetails.class));
    } 
    
 
    public static void main(String args[]){
      
      getInterestRates();
      loanApproved();      
    }
}