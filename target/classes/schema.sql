create table mortgageRate
(
   rate float not null,
   maturityPeriod integer(2) not null,
   currentTime timestamp not null, 
   primary key(rate)
);